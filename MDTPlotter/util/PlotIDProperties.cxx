#include <FourMomUtils/xAODP4Helpers.h>
#include <MDTPlotter/MDTTesterAnalyzer.h>
#include <NtupleAnalysisUtils/NTAUTopLevelIncludes.h>
int main(int argc, char* argv[]) {
    SetAtlasStyle();
    HistoFiller::getDefaultFiller()->setNThreads(8);

    // HistoFiller::getDefaultFiller()->setFillMode(HistoFillMode::singleThread);
    typedef PlotFillInstructionWithRef<TH1, MDTTesterAnalyzer> PlotInstruct;

    const std::string tree_name = "MDTTests";
    std::vector<std::string> in_files{"/mnt/d/CERNBox/group.det-muon/group.det-muon.25463765.MDTOUTSTREAM._000001.root"};
    bool clear_called = false;
    std::string out_dir = "Plots/";  // fallback to NtupleAnalysisUtils default scheme if not specified
    for (int k = 1; k < argc; ++k) {
        std::string current_arg(argv[k]);
        if (current_arg == "--inFile" && k + 1 < argc) {
            if (!clear_called) in_files.clear();
            in_files.push_back(argv[k + 1]);
        } else if (current_arg == "--inDir" && k + 1 < argc) {
            if (!clear_called) in_files.clear();
            in_files += ListDirectory(argv[k + 1], "");
        }

        else if (current_arg == "--outDir" && k + 1 < argc)
            out_dir = argv[k + 1];
    }

    CanvasOptions canvas_opts = CanvasOptions()
                                    .RatioAxis(AxisConfig().Title("w.r.t. all").ExtraTitleOffset(0.4).Min(0.9).Max(1.005))
                                    .YAxis(AxisConfig().Title("Entries").ExtraTitleOffset(0.4).TopPadding(0.7))
                                    // .XAxis(AxisConfig().ExtraTitleOffset(1.3))
                                    .LabelStatusTag("Simulation Internal")
                                    .OutputDir(out_dir);

    auto multi_page = PlotUtils::startMultiPagePdfFile("AllSagittaPlots", canvas_opts);

    std::vector<PlotContent<TH1>> plots;
    Sample<MDTTesterAnalyzer> rel22_sample = make_sample<MDTTesterAnalyzer>(in_files, tree_name);

    Selection<MDTTesterAnalyzer> basic_cut{[](MDTTesterAnalyzer&) { return true; }};

    /// Count the number of ID tracks
    std::shared_ptr<TH1> phi_histo = MakeTH1(PlotUtils::getLinearBinning(-M_PI, M_PI, 60));
    phi_histo->GetXaxis()->SetTitle("#phi [rad]");
    const std::vector<std::pair<float, float>> eta_ranges{{-0.3, -0.2}, {-0.2, -0.1}, {-0.3, -0.05}, {0.05, 0.3}, {-2.5, 0}, {0., 2.5}};

    auto make_instructions = [&phi_histo](const std::pair<float, float>& eta_range) {
        static const auto ID_size_func = [](MDTTesterAnalyzer& t) { return t.n_IDTrks(); };
        const auto ID_phi_func = [](TH1* h, MDTTesterAnalyzer& t, size_t id) { h->Fill(t.IDTracks_phi(id)); };

        const float low_eta = eta_range.first;
        const float high_eta = eta_range.second;
        auto const kine_selection = [low_eta, high_eta](MDTTesterAnalyzer& t, size_t id_trk) {
            const float eta = t.IDTracks_eta(id_trk);
            return low_eta <= eta && eta < high_eta && t.IDTracks_truthType(id_trk) == MCTruthPartClassifier::IsoMuon;
        };
        std::vector<PlotInstruct> instructions;
        /// All ID tracks
        instructions += PlotInstruct(ID_size_func, kine_selection, ID_phi_func, phi_histo);
        /// ID tracks matched to a reconstructed muon
        instructions += PlotInstruct(
            ID_size_func,
            [kine_selection](MDTTesterAnalyzer& t, size_t id_trk) {
                size_t mu_idx = t.get_muon_assoc_IDTrk(id_trk);
                return kine_selection(t, id_trk) && mu_idx < t.n_muons();
            },
            ID_phi_func, phi_histo);
        /// A reconstructed muon which is Calo Tag
        instructions += PlotInstruct(
            ID_size_func,
            [kine_selection](MDTTesterAnalyzer& t, size_t id_trk) {
                size_t mu_idx = t.get_muon_assoc_IDTrk(id_trk);
                return kine_selection(t, id_trk) && mu_idx < t.n_muons() && t.is_muon_author(mu_idx, xAOD::Muon::CaloTag);
            },
            ID_phi_func, phi_histo);
        /// A calo tag muon which also satisfies the ID cuts
        instructions += PlotInstruct(
            ID_size_func,
            [kine_selection](MDTTesterAnalyzer& t, size_t id_trk) {
                size_t mu_idx = t.get_muon_assoc_IDTrk(id_trk);
                return kine_selection(t, id_trk) && mu_idx < t.n_muons() && t.is_muon_author(mu_idx, xAOD::Muon::CaloTag) &&
                       t.muons_passIDCuts(mu_idx);
            },
            ID_phi_func, phi_histo);

        return instructions;
    };
    static std::vector<std::string> legend_items{"All", "Matched to reco #mu", "Matched to reco CT-#mu", "CT #mu passing ID cuts"};
    static std::vector<int> legend_colors{kBlack, kBlue, kRed, kOrange + 2, kGreen - 3};
    static std::vector<int> legend_styles{kFullTriangleUp, kFullTriangleDown, kFullFourTrianglesPlus, kFullDiamond};

    for (auto& eta : eta_ranges) {
        std::vector<PlotInstruct> inst = make_instructions(eta);
        std::vector<Plot<TH1>> pl;
        for (size_t i = 0; i < inst.size(); ++i) {
            pl += Plot<TH1>(
                RunHistoFiller(rel22_sample, basic_cut, inst[i]),
                PlotFormat().LegendTitle(legend_items[i]).LegendOption("PL").MarkerStyle(legend_styles[i]).Color(legend_colors[i]));
        }
        plots += PlotContent<TH1>(pl, {RatioEntry(1, 0), RatioEntry(2, 0), RatioEntry(3, 0)},
                                  {"Z#rightarrow#mu#mu", Form("%.2f < #eta < %.2f", eta.first, eta.second)

                                  },
                                  "Stonjek", multi_page, canvas_opts);
    }

    std::sort(plots.begin(), plots.end(),
              [](const PlotContent<TH1>& a, const PlotContent<TH1>& b) { return a.getFileName() < b.getFileName(); });
    for (auto& pc : plots) {
        if (count_non_empty(pc) == pc.getPlots().size()) DefaultPlotting::draw1D(pc);
    }
    return EXIT_SUCCESS;
}
