#ifndef NTAU__MDTTESTERANALYZER__H
#define NTAU__MDTTESTERANALYZER__H
#include "MDTPlotter/MDTTesterTree.h"
#include "MDTPlotter/Utils.h"

class MDTTesterAnalyzer : public MDTTesterTree {
public:
    MDTTesterAnalyzer(TTree* t);
    /// Returns whether the muon has been reconstructed by a particular author
    /// The author is given by xAOD::Muon::Author
    ///  https://gitlab.cern.ch/atlas/athena/blob/master/Event/xAOD/xAODMuon/xAODMuon/versions/Muon_v1.h#L98-112
    bool is_muon_author(size_t i, int author);
    /// Returns the number of muons in the event
    size_t n_muons();
    /// Returns the number of ID tracks in the event
    size_t n_IDTrks();
    /// Returns the number of ME tracks in the event
    size_t n_METrks();
    /// Returns the number of MS tracks in the event
    size_t n_MSTrks();

    /// Returns a vector of the MDT link
    std::vector<size_t> matched_mdts(int i);

    /// Returns whether the matched MDT is an inner chamber
    bool is_matched_mdt_inner(size_t mdt_idx);
    /// Returns whether the matched MDT is a middle chamber
    bool is_matched_mdt_middle(size_t mdt_idx);
    /// Returns whether the matched MDT is an outer chamber
    bool is_matched_mdt_outer(size_t mdt_idx);
    /// Returns whether the matched MDT is a hole
    bool is_matched_mdt_hole(size_t mdt_idx);
    /// Returns whether the matched MDT is an outlier
    bool is_matched_mdt_outlier(size_t mdt_idx);
    /// Returns whether the matched MDT is on-track
    bool is_matched_mdt_ontrack(size_t mdt_idx);

    /// Returns the index of the muon matched to the given ID track
    size_t get_muon_assoc_IDTrk(size_t id_idx);
    /// Returns the index  of the muon matched to the given ME track
    size_t get_muon_assoc_METrk(size_t me_idx);
    /// Returns the index of the muon matched to the given MS track
    size_t get_muon_assoc_MSTrk(size_t ms_idx);

private:
    bool is_matched_mdt(size_t, int trk_bit);
    size_t get_assoc_muon(NtupleBranch<std::vector<Short_t>>& link_branch, const int idx);
};
#endif