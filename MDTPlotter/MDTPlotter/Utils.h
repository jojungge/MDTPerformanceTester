#ifndef MDTPLOTTER__UTILS_H
#define MDTPLOTTER__UTILS_H
#include <NtauHelpers/HistoUtils.h>
#include <NtauHelpers/MuonUtils.h>
#include <NtauHelpers/Utils.h>

#include "MCTruthClassifier/MCTruthClassifierDefs.h"

// Copied from https://gitlab.cern.ch/atlas-mcp/MuonTester/-/blob/master/MuonTester/MuonTester/Utils.h
// MDT functions
namespace MDTIndex {
    enum StationIndex {
        BIL = 1,
        BIS = 2,
        BML = 3,
        BMS = 4,
        BOL = 5,
        BOS = 6,
        BEE = 7,
        BIM = 8,
        BIR = 9,
        BMF = 10,
        BOF = 11,
        BOG = 12,
        EIL = 13,
        EIS = 14,
        EML = 15,
        EMS = 16,
        EOL = 17,
        EOS = 18,
        EEL = 19,
        EES = 20,
        BME = 21,
        BOE = 22,
        BMG = 23,
        Undefined = -1
    };
}
namespace TrackSummary {
    enum TrackHitType {
        Inner = 1 << 0,
        Middle = 1 << 1,
        Outer = 1 << 2,
        Extended = 1 << 3,
        Hole = 1 << 4,
        OnTrack = 1 << 5,
        Outlier = 1 << 6,
    };

}

#endif