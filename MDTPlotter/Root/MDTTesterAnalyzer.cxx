#include "MDTPlotter/MDTTesterAnalyzer.h"

MDTTesterAnalyzer::MDTTesterAnalyzer(TTree* t) : MDTTesterTree(t) {}

bool MDTTesterAnalyzer::is_muon_author(size_t i, int author) { return muons_allAuthors(i) & (1 << author); }

std::vector<size_t> MDTTesterAnalyzer::matched_mdts(int i) {
    std::vector<size_t> matched;
    size_t n_matched_chambers = muons_trkMdtLink.size();
    matched.reserve(n_matched_chambers);
    for (size_t n = 0; n < n_matched_chambers; ++n) {
        if (muons_trkMdtLink(n) == i) matched.push_back(n);
    }
    return matched;
}

bool MDTTesterAnalyzer::is_matched_mdt_inner(size_t mdt_idx) { return is_matched_mdt(mdt_idx, TrackSummary::Inner); }
bool MDTTesterAnalyzer::is_matched_mdt_middle(size_t mdt_idx) { return is_matched_mdt(mdt_idx, TrackSummary::Middle); }
bool MDTTesterAnalyzer::is_matched_mdt_outer(size_t mdt_idx) { return is_matched_mdt(mdt_idx, TrackSummary::Outer); }
bool MDTTesterAnalyzer::is_matched_mdt_hole(size_t mdt_idx) { return is_matched_mdt(mdt_idx, TrackSummary::Hole); }
bool MDTTesterAnalyzer::is_matched_mdt_outlier(size_t mdt_idx) { return is_matched_mdt(mdt_idx, TrackSummary::Outlier); }
bool MDTTesterAnalyzer::is_matched_mdt_ontrack(size_t mdt_idx) { return is_matched_mdt(mdt_idx, TrackSummary::OnTrack); }
bool MDTTesterAnalyzer::is_matched_mdt(size_t mdt_idx, int trk_bit) { return trkMdt_HitType(mdt_idx) & trk_bit; }

size_t MDTTesterAnalyzer::n_muons() { return muons_q.size(); }
size_t MDTTesterAnalyzer::n_IDTrks() { return IDTracks_q.size(); }
size_t MDTTesterAnalyzer::n_METrks() { return METracks_q.size(); }
size_t MDTTesterAnalyzer::n_MSTrks() { return MSTracks_q.size(); }

size_t MDTTesterAnalyzer::get_assoc_muon(NtupleBranch<std::vector<Short_t>>& link_branch, const int idx) {
    std::vector<Short_t>::const_iterator itr =
        std::find_if(link_branch.begin(), link_branch.end(), [&idx](const Short_t& mu) { return mu == idx; });
    if (itr != link_branch.end()) return (*itr);
    return link_branch.size();
}
size_t MDTTesterAnalyzer::get_muon_assoc_IDTrk(size_t id_idx) { return get_assoc_muon(muons_IDLink, id_idx); }
size_t MDTTesterAnalyzer::get_muon_assoc_METrk(size_t me_idx) { return get_assoc_muon(muons_MELink, me_idx); }
size_t MDTTesterAnalyzer::get_muon_assoc_MSTrk(size_t ms_idx) { return get_assoc_muon(muons_MSLink, ms_idx); }
